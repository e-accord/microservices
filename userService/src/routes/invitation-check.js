import express from "express";
import jwt from "jsonwebtoken";
import Team from './../schema/team';
import User from './../schema/user';
import mongoose from 'mongoose';
import {invitationConfirmed} from './../event/actions/invitation';
import Invitation from './../schema/invitation';

const router = express.Router();

router.post('/', (req, res) => {
    const confirmationToken = req.body.token;
    const dispatch = req.app.get('dispatch');
    jwt.verify(confirmationToken, process.env.JWT_SECRET, (err, data) => {

        if (err) {
            return res.status(403).send('Authorization faild');
        }

        Invitation.findOne({confirmationToken}, (err, invitation) => {
            if(err) {
                return res.status(400).send('Invalid invitation');
            }

            if (invitation.allredyValidated()){
                return res.status(400).send('Invitation allredy validated');
            }

        })

        Team.findById(data.teamId, (err, team) => {

            if(err || null == team){
                return res.status(400).send('Invalid team');
            }

            const member = new User({
                _id: mongoose.Types.ObjectId(),
                email: data.email,
                firstName: data.name
            })

            member.generatePassword()

            member.save((err, user) => {
                if(!err){
                    
                    Team.updateOne({ _id: team._id }, { members: [...team.members, user._id]}, err => {
                        if(!err){
                            Invitation.updateOne({ confirmationToken }, { confirmationDate: Date.now()})
                            dispatch(invitationConfirmed(team))

                            return res.status(200).json({
                                teamName: team.name,
                                name: data.name,
                                email: data.email
                            });
                        }
                    })

                }
            })

        })

    })

});

export default router;