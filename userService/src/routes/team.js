import express from "express";
import mongoose from 'mongoose';
import requireAuth from './../middleware/require-auth';
import Team from './../schema/team';

const router = express.Router();
router.use(requireAuth);

router.post('/', (req, res) => {
    Team.create(
        {
            _id: mongoose.Types.ObjectId(),
            name: req.body.name,
            owner: req.currentUser._id
        },
        (err, team) => {

            if(err) {
                res.status(400).json(err)
            }

            return res.status(200).json(team);
        }
    )
});

export default router;