import express from "express";
import requireAuth from './../middleware/require-auth';
import Invitation from './../schema/invitation';
import Team from './../schema/team';
import mongoose from 'mongoose';
import { newInvitation } from './../event/actions/invitation';
import url from 'url';

const router = express.Router();
router.use(requireAuth);

router.post('/', (req, res) => {
    const referer = req.get('Referer');
    const parsedUrl = url.parse(referer);
    const dispatch = req.app.get('dispatch');
    if(req.body.guests)
    {
        req.body.guests.forEach(invitation => {

            const { name, email } = invitation;
            const {teamId} = req.body;
            
            Team.findById(teamId, (err, team) => {

                if(err){
                    return res.status(400).send('Invalid team');
                }
                
                const InvitationModel = new Invitation(
                    {
                        name, email, 
                        team: team.id,
                        _id: mongoose.Types.ObjectId()
                    }
                );
                InvitationModel.generateConfirmationToken()
                InvitationModel.save( (err, invitation) => {
                    if(!err){
                        const data = { 
                            name: invitation.name, 
                            email: invitation.email, 
                            confirmationToken: invitation.confirmationToken,
                            host: parsedUrl.host
                        }
                        dispatch( newInvitation(data) )
                            .catch(err => console.log(err))
                    }
                } );
               
            })

        });

        return res.status(200).send('OK')
    }

    return res.status(400).send('KO')
});

export default router;