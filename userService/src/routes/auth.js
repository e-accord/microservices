import express from "express";
import User from './../schema/user';
import jwt from "jsonwebtoken";

const router = express.Router();

router.post('/', (req, res) => {
    const credentials = req.body;
    User.findOne({email: credentials.email}, (err, user) => {

        if(!user){
            return res.status(400).json({ message: 'invalid email'})
        }

        if(user.checkPassword(credentials.password)){
            return res.status(200).json(
                {
                    token: user.getToken()
                }
            );
        }

        return res.status(400).json({message: 'invalid email/password'})
    })
});

router.all('/check', (req, res) => {
    const header = req.headers.authorization;
    
    if (!header) {
        return res.status(403).send('Authorization faild');
    }
    
    const token = header.split(" ")[1];
    
    if(!token) {
        return res.status(403).send('Authorization faild');
    }
    
    jwt.verify(token, process.env.JWT_SECRET, err => {
        if(err) {
            return res.status(403).send('Authorization faild');
        }
    })

    return res.status(200).send('OK');
});                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         

export default router;