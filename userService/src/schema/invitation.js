import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';

const invitationSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    name: {
        type: String
    },
    confirmationToken: { 
        type: String, 
        default: "" 
    },
    confirmationDate: {
        type: Number,
        default: null
    },
    team: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});

invitationSchema.methods.generateConfirmationToken = function () {
    this.confirmationToken = jwt.sign(
        {
            email: this.email,
            name: this.name,
            teamId: this.team
        },
        process.env.JWT_SECRET,
        {
            expiresIn: '1d'
        }
    )
    
}

invitationSchema.methods.allredyValidated = function () {
    return null !== this.confirmationDate;
}

export default mongoose.model('Invitation', invitationSchema);