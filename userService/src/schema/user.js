import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const userSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    passwordHash: String,
    firstName: String,
    lastName: String
});

userSchema.methods.checkPassword = function (password) {
    console.log(bcrypt.hashSync(password, 15));
    
    
    return bcrypt.compareSync(password, this.passwordHash);
}

userSchema.methods.setPassword = function (password) {
    this.passwordHash = bcrypt.hashSync(password, 15);
}

userSchema.methods.generatePassword = function () {
    const random = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    this.setPassword(random);
}

userSchema.methods.getToken = function () {
    return jwt.sign(
        {
            email: this.email
        },
        process.env.JWT_SECRET,
        {
            expiresIn: '1h' 
        }
    )
}

export default mongoose.model('User', userSchema);

export {userSchema};