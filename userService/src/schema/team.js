import mongoose from 'mongoose';

const teamSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    members: {
        type: [mongoose.Schema.Types.ObjectId]
    },
    apis: {
        type: [mongoose.Schema.Types.ObjectId]
    }
});

export default mongoose.model('Team', teamSchema);