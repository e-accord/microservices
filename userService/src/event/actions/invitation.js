
const newInvitation = invitation => (
    {
        routingKey: 'invitation',
        exchangeName: 'emails',
        data: invitation
    }
)

const invitationConfirmed = data => (
    {
        routingKey: 'invitation-confirmed',
        exchangeName: 'emails',
        data
    }
)

export { newInvitation, invitationConfirmed }