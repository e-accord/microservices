import bodyParser from 'body-parser';
import amqp from 'amqplib';

const publish = (channel, { routingKey, exchangeName, data }) => {
    return new Promise( (resolve, reject) => {
        channel.publish(
            exchangeName, 
            routingKey, 
            Buffer.from(JSON.stringify(data), 
            'utf-8', 
            {persistent: true},
            (err, ok) => {
                if(err){
                    return reject(err);
                }

                resolve(channel);
            })
        )
    })
}

const dispatchMiddleware = async (res, req, next) => {;
    
    const connection = await amqp.connect({
        hostname: process.env.RABBITMQ_HOST,
        vhost: process.env.RABBITMQ_VHOST
    });
    
    const channel = await connection.createConfirmChannel();
    
    req.app.set(
        'dispatch', 
        async ({ routingKey, exchangeName, data }) => {
        return publish(channel, { routingKey, exchangeName, data });
    });
   
    next();
}

export { dispatchMiddleware };