import express from 'express';
import auth from './routes/auth'
import invitation from './routes/invitation';
import invitationCheck from './routes/invitation-check';
import team from './routes/team';
import mongoose from "mongoose";
import dotenv from "dotenv";
import { Promise } from "bluebird";
import { dispatchMiddleware } from './event/handler';

dotenv.config()
const app = express();
mongoose.connect(process.env.MONGODB_URL);
mongoose.Promise = Promise;

app.use(express.json()) // for parsing application/json
app.use(dispatchMiddleware);

app.use('/users/auth', auth);
app.use('/users/invitation/validate', invitationCheck);
app.use('/users/invitation', invitation);
app.use('/users/team', team);

app.get('/users/*', (req, res) => {
    res.send('API CREATOR USER SERVICE');
})

app.listen(process.env.LISTEN_PORT);