import express from "express";
import Api from "./../schema/api";
import mongoose from "mongoose";

const router = express.Router();

router.post('/', (req, res) => {
    Api.create({ ...req.body, _id: mongoose.Types.ObjectId() }, (err, api) => {
        if(err){
            return res.status(400).json(err)
        }

        return res.json(api)
    });
});

router.get('/', (req, res) => {
    Api.find({}).then( apis => res.json(apis) );
});

router.delete('/', (req, res) => {
    Api.deleteOne({...req.params}, err => {
        if(err){
            return res.status(400).json(err)
        }

        return res.status(200).send('deleted');
    })
})

router.get('/:id', (req, res) => {
    Api.findById(req.params.id, (err, api) => {
        
        if (err) {
            return res.status(400).json(err)
        }

        return res.status(200).json(api)
    })
})

router.put('/', (req, res) => {
    Api.update({_id: req.body._id}, {...req.body}, err => {

        if (err) {
            return res.status(400).json(err)
        }

        Api.findById(req.body._id, (err, api) => {
            if (err) {
                return res.status(400).json(err)
            }

            return res.status(200).json(api)
        })
    });
})

export default router;