import express from 'express';
import apis from './routes/apis';
import mongoose from "mongoose";
import dotenv from "dotenv";
import { Promise } from "bluebird";

dotenv.config()
const app = express();
mongoose.connect(process.env.MONGODB_URL);
mongoose.Promise = Promise;

app.use(express.json()) // for parsing application/json

app.use('/apis', apis);

app.get('/*', (req, res) => {
    res.send('API CREATOR API SERVICE');
})

app.listen(process.env.LISTEN_PORT);