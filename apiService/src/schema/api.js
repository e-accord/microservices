import mongoose from 'mongoose';

const apiSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: String
});

export default mongoose.model('Api', apiSchema);

