import amqp from 'amqplib';
import dotenv from "dotenv";

dotenv.config()
async function setup() {
    console.log("Setting up RabbitMQ Exchanges/Queues for EMAIL SERVICE");

    const connection = await amqp.connect({
        hostname: process.env.RABBITMQ_HOST,
        vhost: process.env.RABBITMQ_VHOST
    })

    // create a channel
    let channel = await connection.createChannel();

    // create exchange
    await channel.assertExchange("emails", "direct", { durable: true });

    // create queues
    await channel.assertQueue("invitations", { durable: true });

    // bind queues
    await channel.bindQueue("invitations", "emails", "invitation");

    console.log("Setup DONE");
    process.exit();
}

setup();