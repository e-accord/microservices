import nodemailer from "nodemailer";

export default () => {
    return nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT
    });
}