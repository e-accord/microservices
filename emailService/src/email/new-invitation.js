import setup from './setup';

export default (email, confirmationToken, clientHost = 'http://localhost:3000') => {
    const tranport = setup();
    const emailData = {
        from: process.env.EMAIL_FROM,
        to: email,
        subject: "Welcome to Reconcile",
        text: `Welcome to Reconcile. Please, confirm your email. http://${clientHost}/confirm-invitation/${confirmationToken}`
    };

    tranport.sendMail(emailData);
}