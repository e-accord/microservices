const INVITATION_QUEUE = 'invitations';

const consume = ({ connection, channel }, queue, callback) => {

    channel.consume(queue, async (msg) => {
        const msgBody = msg.content.toString();
        const data = JSON.parse(msgBody);
        await channel.ack(msg);

        callback(null, data);
    });

    // handle connection closed
    connection.on("close", (err) => {
        return callback(err, null);
    });

    // handle errors
    connection.on("error", (err) => {
        return callback(err, null);
    });

}

const consumeNewInvitation = ({ connection, channel }, callback) => consume({connection, channel}, INVITATION_QUEUE, callback);

export { consumeNewInvitation }