# EMAIL SERVICE

This service is responsible for email exchange and handling . It listen from message sent to *email* rabbitmq exchange and send email acordigly .

## setup

Since every single service is responsible from it's rabbitmq exchange configuration, we have to run :
```
$npm run setup
```

This script will create all needed rabbitmq exchanges and queues, and also excecute binding.

### exchanges:
1) *emails*: publishers will need to send msg to this exchange in order to trigger any sending email

### queue:
1) *invitations*: this will be bind with *emails* exchange with de routing key **invitation**

notice that all routing keys are the single form of it's queue binding on .

****

**warning:** rabbitmq service must be up and available before running previous command

### dotenv configurations :
default:

RABBITMQ_HOST=rabbitmq

RABBITMQ_VHOST=/

EMAIL_HOST=mailer

EMAIL_PORT=1025

EMAIL_FROM="Reconcile" <info@reconcile.com>