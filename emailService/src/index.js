import amqp from 'amqplib';
import {consumeNewInvitation} from './consumer/consumer';
import dotenv from "dotenv";
import sendConfirmationEmail from './email/new-invitation';

dotenv.config()

const run = async () => {
    
    const connection = await amqp.connect({
        hostname: process.env.RABBITMQ_HOST,
        vhost: process.env.RABBITMQ_VHOST
    })

    const channel = await connection.createChannel();
    await channel.prefetch(1);
 
    consumeNewInvitation(
        {connection, channel},
        (err, data) => {
            if(!err){
                return sendConfirmationEmail(data.email, data.confirmationToken, data.host);
            }
        }
    )
}

run();