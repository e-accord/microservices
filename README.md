# Reconcile

> The API Shandbox creator

This app was desinged with the *microservice* architecture using rabbitmq as message broker that allow the internal communications .

# Services

Since microservice architecture stand for spliting the application into multiple small independent applications, every busines concept of reconcile will be a ready-to-deploy service .

## NGINX the api gateway

Services can expose his busines through an api and each services have it's own api but these apis will not be directly accesible from the outside world . Infact, if you look to the *docker-compose.yml* file there is no ports mapping from users service or apis service .

To allow access to a specific microservice Nginx act as reverse proxy and redirect internaly each request he receave . This allow us to have an *api gateway* that is common for all client outside the application (becose yeah the backend part is the real application)

NGINX make also easy the authorization mechanism through the authentification request . For a specific request nginx will make a sub request, if the sub request return 200 as status the primary request will be processed othertwise a 403 response will be send .

## Configurating a new service

First inside api/backend.conf define new upstream that will have as server the host of related service, then create new file from api/conf.d and add new location with the newly created upstream .

```
# api/backend.conf

upstream reconcile_users {
    server users:3000;
}

# api/conf.d/users.conf

location /users {
    set $upstream reconcile_users;
    rewrite ^ /_reconcile last;
}
```

In the code bellow the location **/users** will be rewrite internaly to **/_reconcile** and from this location a proxy pass will be configured to the defined upstream (see api/gateway.conf) .

If you need the location to be authorized, rewrite to **/_reconcile_private** instead . In this case nginx will make a sub request to http://users:3000/users/auth/check for authorization .

## Rabbitmq configuration

Each single microservice app is responsible from configurating the needed *exchange queue and binding key* from it's own mechanism . For example emails service will start with the cmd

```
CMD npm run setup && npm start
```

where the script **setup** will create all needed configuration for triggering email through rabbitmq messages .

A web ui is also available from http://localhost:15672 for a more friendly and manual configurations .

## Launch the app

Since many service depend on rabbitmq service, the safe way to launch the app is starting rabbitmq service in firstime .

```
$docker-compose up -d rabbitmq
```
then start the rest

```
$docker-compose up -d # you can omit the -d option to see all logs
```

